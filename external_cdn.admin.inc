<?php

function external_cdn_list_form($form, &$form_state) {
  $form['#tree'] = TRUE;
  $form['#theme'] = 'external_cdn_list_form';
  $data = variable_get('external_cdn_data', array());
  uasort($data, 'drupal_sort_weight');
  foreach ($data as $id => $item) {
    $form['externals'][$id]['name'] = array(
      '#type' => 'item',
      '#markup' => $item['name'],
    );
    $show = isset($item['show']) ? external_cdn_get_show_options($item['show']) : '';    
    $status = isset($item['status']) && $item['status'] == EC_STATUS_ENABLED ? t('Enabled') : t('Disabled');    
    $form['externals'][$id]['show'] = array(
      '#type' => 'item',
      '#markup' => $show,
    );
    $form['externals'][$id]['status'] = array(
      '#type' => 'item',
      '#markup' => $status,
    );
    $form['externals'][$id]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $item['name'])),
      '#title_display' => 'invisible',
      '#default_value' => $item['weight'],
    );    
  }
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Reorder'),    
  );

  return $form;
}

function external_cdn_list_form_submit($form, &$form_state) {
  $item_weights = $form_state['values']['externals'];
  $data = variable_get('external_cdn_data', array());
  foreach ($data as $key => $item)  {
    $data[$key]['weight'] = $item_weights[$key]['weight'];
  }  
  variable_set('external_cdn_data', $data);
  drupal_set_message(t('The CDN items have been reordered'));
}

function external_cdn_visibility_form($form, &$form_state, $id) {
  $data = variable_get('external_cdn_data', array());
  if (!isset($data[$id])) {
    drupal_access_denied();
  }
  $item = $data[$id];
  $form['id'] = array(
    '#type' => 'value',
    '#default_value' => $id,    
  );
  $form['show'] = array(
    '#title' => t('Show'),
    '#type' => 'select',
    '#options' => external_cdn_get_show_options(),
    '#default_value' => $item['show'],
  );  
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  $form['pages_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show CDN on specific pages'),
    '#options' => array(
      EC_VISIBILITY_NOTLISTED => t('All pages except those listed'),
      EC_VISIBILITY_LISTED => t('Only the listed pages'),
    ),
    '#default_value' => isset($item['pages_visibility']) ? $item['pages_visibility'] : EC_VISIBILITY_NOTLISTED,
  );
  $form['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => isset($item['pages']) ? $item['pages'] : '',
    '#description' => $description,
  );
  $form['types_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show CDN for specific content types'),
    '#options' => array(
      EC_VISIBILITY_LISTED => t('Only the checked content types'),
      EC_VISIBILITY_NOTLISTED => t('All content types except those checked'),      
    ),
    '#default_value' => isset($item['types_visibility']) ? $item['types_visibility'] : EC_VISIBILITY_LISTED,
  );
  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => isset($item['types']) ? $item['types'] : array(),
    '#options' => node_type_get_names(),
    '#description' => t('Show this CDN only on pages that display content of the given type(s). If you select no types, there will be no type-specific limitation.<br/> <strong>Note:</strong><ul><li>If you select <strong>Show</strong> as <em>All pages</em> - <strong>node/*</strong>, <strong>node/add/*</strong>, <strong>node/*/edit</strong> pages will be affected for a specific content type;</li><li>If you select <strong>Show</strong> as <em>Admin pages only</em> - <strong>node/add/*</strong>, <strong>node/*/edit</strong> pages will be affected for a specific content type;</li><li> If you select <strong>Show</strong> as <em>User pages only</em> - <strong>node/*</strong> page will be affected for a specific content type</li></ul>'),
  );
  $form['save'] = array(
    '#value' => t('Save'),   
    '#type' => 'submit',     
  );

  return $form;
}

function external_cdn_visibility_form_submit($form, &$form_state) {
  $id               = $form_state['values']['id'];
  $show             = $form_state['values']['show'];
  $pages_visibility = $form_state['values']['pages_visibility'];
  $types_visibility = $form_state['values']['types_visibility'];
  $pages            = $form_state['values']['pages'];
  $content_types    = $form_state['values']['content_types'];
  
  $data = variable_get('external_cdn_data', array()); 
  $data[$id]['show'] = $show;
  $data[$id]['pages_visibility'] = $pages_visibility;
  $data[$id]['types_visibility'] = $types_visibility;
  $data[$id]['pages'] = $pages;
  $data[$id]['types'] = $content_types;

  variable_set('external_cdn_data', $data);
  drupal_set_message(t('The item has been saved'));  
}

function external_cdn_item_form($form, &$form_state, $op) {
  $id = '';
  $name = '';
  $weight = '';
  $css = '';
  $css_min = '';
  $js = '';
  $js_min = '';
  $show = 'user_pages';
  $status = EC_STATUS_ENABLED;

  if ($op !== 'add') {
    $id = $op;
    $op = 'edit';        
    $data = variable_get('external_cdn_data', array());
    extract($data[$id]);    
  }
  $form['form_op'] = array(
    '#type' => 'value',
    '#default_value' => $op,    
  );
  $form['id'] = array(
    '#type' => 'value',
    '#default_value' => $id,    
  );
  $form['name'] = array(
    '#title' => t('Name'),  
    '#type' => 'textfield',
    '#default_value' => $name,    
    '#required' => TRUE,
  );  
  $form['show'] = array(
    '#title' => t('Show'),  
    '#type' => 'select',
    '#options' => external_cdn_get_show_options(),
    '#default_value' => $show,        
  );
  $form['css'] = array(
    '#title' => t('.css'),  
    '#type' => 'textfield',
    '#default_value' => $css,    
    '#size' => 60,
    '#maxlength' => 128,
  );
  $form['css_min'] = array(
    '#title' => t('.min.css'),    
    '#type' => 'textfield',
    '#default_value' => $css_min,    
    '#description' => t('Additionally, you can provide the minimized version of the file. It will be used instead if site aggregation is enabled.'),
    '#size' => 60,
    '#maxlength' => 128,
  );
  $form['js'] = array(
    '#title' => t('.js'),    
    '#type' => 'textfield',
    '#default_value' => $js,    
    '#size' => 60,
    '#maxlength' => 128,
  );
  $form['js_min'] = array(
    '#title' => t('.min.js'),
    '#type' => 'textfield',
    '#default_value' => $js_min,      
    '#description' => t('Additionally, you can provide the minimized version of the file. It will be used instead if site aggregation is enabled.'),
    '#size' => 60,
    '#maxlength' => 128,
  );
  $form['status'] = array(
    '#title' => t('Status'),  
    '#type' => 'radios',
    '#options' => array(
      EC_STATUS_ENABLED => t('Enabled'),
      EC_STATUS_DISABLED => t('Disabled'),
    ),
    '#default_value' => $status,        
  ); 
  $form['weight'] = array(
    '#title' => t('Weight'), 
    '#type' => 'weight',
    '#default_value' => $weight,      
  );

  $form['save'] = array(
    '#value' => t('Save'),   
    '#type' => 'submit',     
  );

  return $form;
}

function external_cdn_item_form_submit($form, &$form_state) {
  $op = $form_state['values']['form_op'];
  $id = $form_state['values']['id'];  
  $name = $form_state['values']['name'];
  $weight = $form_state['values']['weight'];
  $css = $form_state['values']['css'];
  $css_min = $form_state['values']['css_min'];
  $js = $form_state['values']['js'];
  $js_min = $form_state['values']['js_min'];
  $show = $form_state['values']['show'];
  $status = $form_state['values']['status'];
  
  $data = variable_get('external_cdn_data', array());    
  if ($op === 'add') {
    $element = array(
      'name' => $name, 
      'weight' => $weight, 
      'css' => $css, 
      'css_min' => $css_min, 
      'js' => $js, 
      'js_min' => $js_min, 
      'show' => $show,
      'status' => $status,
      'pages_visibility' => EC_VISIBILITY_NOTLISTED,
      'types_visibility' => EC_VISIBILITY_LISTED,
      'pages' => '',
      'types' => array()
    );
    $data[] = $element;
  } else {
    $data[$id]['name'] = $name;
    $data[$id]['weight'] = $weight;
    $data[$id]['css'] = $css;
    $data[$id]['css_min'] = $css_min;
    $data[$id]['js'] = $js;
    $data[$id]['js_min'] = $js_min;
    $data[$id]['show'] = $show;
    $data[$id]['status'] = $status;
  }
    
  variable_set('external_cdn_data', $data);
  drupal_set_message(t('The item has been saved'));
}

/**
 * Delete confirmation form.
 */
function external_cdn_item_delete_confirm_form($form, &$form_state, $id) {    
  $data = variable_get('external_cdn_data', array());
  $name = $data[$id]['name'];
  $form_state['item_id'] = $id; 
  return confirm_form($form,
    t('Are you sure you want to delete %name CDN?', array('%name' => $name)),
    'admin/config/development/external_cdn',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function external_cdn_item_delete_confirm_form_submit($form, &$form_state) {
  $id = $form_state['item_id'];
  $data = variable_get('external_cdn_data', array());
  unset($data[$id]);
  variable_set('external_cdn_data', $data);
  drupal_set_message(t('The CDN item has been deleted'));  
}